/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Auteur : Vincent Limorté
 * Mars 2017
 */

#include "AnalogRead.h"


int AnalogRead::readValue() {
	if(millis()>_lastReadTime+_readInterval){
		_currentValue=analogRead(_pin);
		_lastReadTime=millis();
		// Lecture étant faite, on met à jour le buffer de valeurs :
		_rawValues[_idx%_nbLectures]=_currentValue;
		_idx++;
	}
	return _currentValue;
}

int AnalogRead::getValue() {
	readValue();
	// on prend la moyenne:
	int somme=0;
	for(int i=0;i<_nbLectures;i++){
		somme+=_rawValues[i];
	}
	int moyenne = somme/_nbLectures;
	return  map(moyenne, 0, 1024, _min, _max);
}

AnalogRead::AnalogRead(uint8_t pin, int min, int max, int nbLectures, unsigned long readInterval) {
	_pin=pin;
	_min=min;
	_max=max+1;
	_currentValue=0;
	_lastValue=0;
	_nbLectures=nbLectures;
	_readInterval=readInterval;
	_lastReadTime=0;
	_idx=0;
	_rawValues = new int[_nbLectures];
	pinMode(_pin, INPUT);
	int primeVal = readValue();
	// Remplit le buffer de lecture
	for(int i=0;i<_nbLectures;i++){
		_rawValues[i]=primeVal;
	}
}

AnalogRead::~AnalogRead() {
	delete [] _rawValues;
}

