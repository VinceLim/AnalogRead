/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Auteur : Vincent Limorté
 * Mars 2017
 */

#ifndef ANALOG_READ_H_
#define ANALOG_READ_H_

#define ANALOG_READ_VERSION "v0.0.1"

#include "Arduino.h"

class AnalogRead {
private:
	int _min, _max, _lastValue, _currentValue;
	uint8_t _pin;
	int _nbLectures;
	int* _rawValues;
	int _idx;
	unsigned long _readInterval, _lastReadTime;
	int readValue();

public:
	/**
	 * Renvoie la dernière valeur lue
	 */
	int getValue();
	/**
	 * Lecture d'un potentiomètre branché sur la borne pin
	 * La lecture du pin est ramenée entre les valeur min et max, proportionnellement.
	 * Les lectures se font sur une moyenne de nbLectures.
	 * Chaque lecture se fait tous les readIntervall au minimum (dépendant de l'appel)
	 */
	AnalogRead(uint8_t pin, int min, int max, int nbLectures, unsigned long readIntervall=100);
	virtual ~AnalogRead();
};

#endif /* ANALOG_READ_H_ */
